package com.sabake.cin.trythisone

import android.Manifest
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.graphics.drawable.Drawable
import android.graphics.Bitmap
import android.os.Environment
import android.os.Environment.getExternalStorageDirectory
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception
import android.app.WallpaperManager
import android.graphics.BitmapFactory
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), View.OnClickListener {
    private val images = ArrayList<String>()
    private var pos = 0
    private lateinit var img1: Bitmap
    private lateinit var img2: Bitmap
    private lateinit var img3: Bitmap
    private var buff = ArrayList<Bitmap>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addimages()
        initBuff()
        viewPager.adapter = MyPagerAdapter(supportFragmentManager)
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {/*
                var i = 0
                iv_left.setImageBitmap(buff[i])
                if (position != 0) i += 1
                iv_center.setImageBitmap(buff[i])

                if (position != images.size - 1)
                    Picasso.with(this@MainActivity).load(images[position + 1]).into(object : Target {
                        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                            buff[2] = BitmapFactory.decodeResource(resources, R.drawable.ic_launcher_background)
                        }

                        override fun onBitmapFailed(errorDrawable: Drawable?) {
                            buff[2] = BitmapFactory.decodeResource(resources, R.drawable.ic_launcher_background)
                        }

                        override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom?) {
                            buff[2] = bitmap
                        }
                    })*/

                Picasso.with(this@MainActivity).load(images[position]).into(iv_center)
                if (position == 0) Picasso.with(this@MainActivity).load(images[0]).into(iv_left)
                else Picasso.with(this@MainActivity).load(images[position - 1]).into(iv_left)
                if (position == images.size - 1) Picasso.with(this@MainActivity).load(images[position]).into(iv_right)
                else Picasso.with(this@MainActivity).load(images[position + 1]).into(iv_right)

            }

        })
        btn_download.setOnClickListener(this)
        btn_setwallpapper.setOnClickListener(this)
    }

    private fun initBuff() {
        /*for (i in 0..2)
        {
            buff.add(BitmapFactory.decodeResource(resources, R.drawable.ic_launcher_background))
        }
        for (i in 0..2) {
            Picasso.with(this).load(images[0]).into(object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                    buff[i] = BitmapFactory.decodeResource(resources, R.drawable.ic_launcher_background)
                }

                override fun onBitmapFailed(errorDrawable: Drawable?) {
                    buff[i] = BitmapFactory.decodeResource(resources, R.drawable.ic_launcher_background)
                }

                override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom?) {
                    buff[i] = bitmap
                }
            })
        }*/
        Picasso.with(this).load(images[0]).into(iv_left)
        Picasso.with(this).load(images[0]).into(iv_center)
        Picasso.with(this).load(images[1]).into(iv_right)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            btn_download.id -> {
                btn_download.startAnimation(AnimationUtils.loadAnimation(this, R.anim.click))
                loadImg(images[viewPager.currentItem])
                btns_enabled(false)
            }
            btn_setwallpapper.id -> {
                btn_setwallpapper.startAnimation(AnimationUtils.loadAnimation(this, R.anim.click))
                setWallPapper(images[viewPager.currentItem])
            }
        }
    }

    private fun btns_enabled(state: Boolean) {
        btn_download.isEnabled = state
        btn_setwallpapper.isEnabled = state
    }

    private fun addimages() {
        images.add("https://pp.userapi.com/c627630/v627630517/4897d/usuCDlqW_1I.jpg")
        images.add("https://pp.userapi.com/c627630/v627630517/48b49/Q9uEcMPGBeo.jpg")
        images.add("https://pp.userapi.com/c627630/v627630517/48b67/sBRyDx0Cczw.jpg")
        images.add("https://pp.userapi.com/c627630/v627630517/48b85/jP0CLFml6h4.jpg")
        images.add("https://pp.userapi.com/c627630/v627630517/48b99/c6PLwLP0tGk.jpg")
        images.add("https://pp.userapi.com/c627630/v627630517/48bad/SE37A6kkaz8.jpg")
        images.add("https://pp.userapi.com/c627630/v627630517/48bdf/_OayFOIXMis.jpg")
        images.add("https://pp.userapi.com/c627630/v627630517/48c43/xjcmlaval-k.jpg")
        images.add("https://pp.userapi.com/c627630/v627630517/48c57/KkZToe3_qA8.jpg")
        images.add("https://pp.userapi.com/c627630/v627630517/48c75/LWvV_bzKOIs.jpg")
        images.add("https://pp.userapi.com/c627630/v627630517/49583/hBUYAWrFU-Q.jpg")
        images.add("https://pp.userapi.com/c627630/v627630517/49597/B6372nxqG6M.jpg")
        images.add("https://pp.userapi.com/c627630/v627630517/495bf/jZjD4KC-VGA.jpg")
        images.add("https://pp.userapi.com/c627630/v627630517/497b3/I-3vIoPrmQU.jpg")
        images.add("https://pp.userapi.com/c627630/v627630517/49885/diX1BKuO6I8.jpg")
    }

    fun loadImg(url: String) {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    50)
        }
        Picasso.with(this).load(url).into(object : Target {
            override fun onBitmapFailed(errorDrawable: Drawable?) {
                Log.d("kek", "failur")

            }

            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                Thread(Runnable {
                    var file = File(Environment.getExternalStorageDirectory(), getString(R.string.app_name))
                    if (!file.exists()) {
                        file.mkdirs()
                    }
                    file = File(Environment.getExternalStorageDirectory().getPath() + "/" + getString(R.string.app_name) + "/image" + pos + ".jpg")
                    try {
                        file.createNewFile()
                        val ostream = FileOutputStream(file)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream)
                        ostream.flush()
                        ostream.close()
                        Log.d("succes", url)
                        runOnUiThread {
                            btns_enabled(true)
                            Toast.makeText(this@MainActivity, "Файл загружен", Toast.LENGTH_LONG).show()

                        }

                    } catch (e: IOException) {
                        Log.d("IOException", e.getLocalizedMessage())
                    }
                }).start()

            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                Log.d("kek", "prepare")
            }

        })

    }

    fun setWallPapper(bitmap: Bitmap) {
        val myWallpaperManager = WallpaperManager.getInstance(applicationContext)
        try {
            myWallpaperManager.setBitmap(bitmap)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun setWallPapper(str: String) {
        Picasso.with(this).load(str).into(object : Target {
            override fun onBitmapFailed(errorDrawable: Drawable?) {
                Log.d("kek", "failur")

            }

            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                setWallPapper(bitmap)
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                Log.d("kek", "prepare")
            }

        })
    }

    inner class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            pos = position
            return BlankFragment.newInstance(images[position])
        }

        override fun getCount(): Int {
            return images.size
        }

    }
}
