package com.sabake.cin.trythisone


import android.graphics.*
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_blank.*
import kotlinx.android.synthetic.main.fragment_blank.view.*
import android.opengl.ETC1.getHeight
import android.opengl.ETC1.getWidth
import com.squareup.picasso.Transformation


/**
 * A simple [Fragment] subclass.
 *
 */
class BlankFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_blank, container, false)
        Picasso.with(context).load(arguments!!.getString("url")).into(v.img_main)
        return v
    }


    companion object {
        fun newInstance(url: String): BlankFragment {
            val args: Bundle = Bundle()
            args.putString("url", url)
            val fragment = BlankFragment()
            fragment.arguments = args
            return fragment
        }
    }
}

